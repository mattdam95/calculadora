const buttons = document.querySelectorAll("BUTTON");
const screen_oper = document.querySelector(".screen_oper");
const screen_result = document.querySelector(".screen_result");

let numeroIngresado = [];
let operIn = "";
let operFin = "=";
let operResult = false;
let result = false;
let numeroDos = false;
let numeroUno = false;
let numeroTres = false;
let oper = [];

eventListener();

function eventListener() {
  buttons.forEach((button) => {
    button.addEventListener("click", operaciones);
  });
  console.log(2/0);
}

function operaciones(e) {
  let tecla = e.target.outerText;
  limpiarScreenResult();

  if (tecla === "C") {
    limpiarOperacion(e);
    return;
  }

  if (tecla === "<") {
    borrar(e);
    return;
  }

  if (limitarOperandos(e)) {
    resultScreen(numeroIngresado.join(""));
    return;
  }

  operResult = false;
  let num = numeros(e);

  if (operFin === "=") {
    if (num) {
      if (numeroIngresado.includes(".") && tecla === ".") {
        resultScreen(numeroIngresado.join(""));
        return;
      }
      if (numeroIngresado.length > 15) {
        resultScreen(numeroIngresado.join(""));
        return;
      }
      numeroIngresado.push(tecla);
      resultScreen(numeroIngresado.join(""));
      operScreen(oper);
    } else {
      if (!numeroUno) {
        operIn = tecla;
        numeroUno = parseFloat(numeroIngresado.join(""));
        if (isNaN(numeroUno)) {
          if (oper.length == 0) {
            oper.push(operIn);
          }
        } else {
          oper.push(numeroUno, operIn);
        }

        operScreen(oper);
        limpiarScreenResult();

        if (operIn === "=") {
          //En el caso que ingrese solo un valor e iguale
          resultScreen(numeroUno);
          oper = [];
          numeroUno = false;
        }

        numeroIngresado = [];
        operResult = tecla;
      } else if (tecla === "=") {
        numeroDos = parseFloat(numeroIngresado.join(""));
        result = operando(numeroUno, numeroDos, operIn);

        //Mostrar en pantalla;
        oper.push(numeroDos, "=");
        operScreen(oper);
        oper = [];

        limpiarScreenResult();
        console.log(result.toString());

        resultScreen(result.toString());

        numeroIngresado = [];
        numeroUno = false;
      } else if (tecla !== "=") {
        numeroDos = parseFloat(numeroIngresado.join(""));
        operFin = tecla;
        result = operando(numeroUno, numeroDos, operIn);

        //Mostrar en pantalla;
        oper = [];
        oper.push(result, operFin);
        operScreen(oper);
        resultScreen(result.toString());

        numeroIngresado = [];
        operResult = tecla;
      }
    }
  } else {
    if (num) {
      if (numeroIngresado.includes(".") && tecla === ".") {
        resultScreen(numeroIngresado.join(""));
        return;
      }
      numeroIngresado.push(tecla);
      resultScreen(numeroIngresado.join(""));
      operScreen(oper);
    } else {
      numeroTres = parseFloat(numeroIngresado.join(""));
      operIn = operFin;
      operFin = tecla;
      result = operando(result, numeroTres, operIn);

      //Mostrar en pantalla;
      if (operFin === "=") {
        oper.push(numeroTres, operFin);
        operScreen(oper);
        resultScreen(result);
        oper = [];
        numeroUno = false; //Reincia n1 lo que reinicia las operaciones
      } else {
        oper = [];
        oper.push(result, operFin);
        operScreen(oper);
        resultScreen(result);
      }

      numeroIngresado = [];
      operResult = tecla;
    }
  }
}

function numeros(e) {
  const num = parseInt(e.target.outerText);
  //Permite los numeros negativos
  if (
    numeroIngresado.length === 0 &&
    numeroUno === false &&
    (e.target.outerText === "-" || e.target.outerText === "+")
  ) {
    return true;
  }
  if (e.target.outerText === ".") {
    //Permite el . en numeros
    return true;
  } else if (isNaN(num)) {
    return false;
  }
  return true;
}

function operando(numUno, numDos, operIn) {
  if(numDos === 0){
    return 'Error';
  }
  switch (operIn) {
    case "+":
      return numUno + numDos;
    case "-":
      return numUno - numDos;
    case "*":
      return numUno * numDos;
    case "/":
      return numUno / numDos;
    case "%":
      return (numUno / 100) * numDos;
    case "=":
      return numUno;
    default:
      break;
  }
}

function limpiarOperacion() {
  //Reseta todo los valores por defecto
  numeroIngresado = [];
  operIn = "";
  operFin = "=";
  operResult = false;
  result = false;
  numeroDos = false;
  numeroUno = false;
  numeroTres = false;
  oper = [];
  limpiarScreenOper();
  limpiarScreenResult();
}

function limitarOperandos(e) {
  //Revisa que no haya dos operandos seguidos en op intermedias
  if (operResult !== false && isNaN(parseInt(e.target.outerText))) {
    return true;
  }
  if (
    //Deja pasar + o - cuando sean el primer valor ingresado
    (numeroUno !== false ||
      e.target.outerText === "+" ||
      e.target.outerText === "-") &&
    numeroIngresado.length === 0
  ) {
    return false;
  }
  if (
    //Revisa que no haya dos operandos seguidos en los primeros 2 valores
    isNaN(parseInt(numeroIngresado[0])) &&
    numeroIngresado.length <= 1 &&
    isNaN(parseInt(e.target.outerText))
  ) {
    return true;
  }
}

function borrar(e) {
  if (numeroIngresado.length !== 0) {
    numeroIngresado.pop();
    resultScreen(numeroIngresado.join(""));
  }
}

//Funciones para la pantalla;

function operScreen(num) {
  limpiarScreenOper();
  const resultScreen = document.createElement("p");
  resultScreen.classList.add("screen__numP");

  num.forEach((element) => {
    resultScreen.textContent += element;
  });

  screen_oper.appendChild(resultScreen);
}

function resultScreen(numero) {
  const numeroScreen = document.createElement("p");
  if (
    numero === 'Error' ||
    parseInt(numero) ||
    numero.includes("0") ||
    numero.includes(".") ||
    numero.includes("-") ||
    numero.includes("+")
  ) {
    numeroScreen.classList.add("screen__numR");
    numeroScreen.textContent = numero;
    screen_result.appendChild(numeroScreen);
  }
}

function limpiarScreenOper() {
  while (screen_oper.firstChild) {
    screen_oper.removeChild(screen_oper.firstChild);
  }
}
function limpiarScreenResult() {
  while (screen_result.firstChild) {
    screen_result.removeChild(screen_result.firstChild);
  }
}

//ans?
